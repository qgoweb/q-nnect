sap.ui.define([
	"com/quanto/solutions/qnnect/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function(BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("com.quanto.solutions.qnnect.controller.SpeechDialog", {

		onInit: function() {

			// initialize model
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setData({
				console: "",
				consoleVisible: true
			});
			sap.ui.getCore().setModel(oModel, "speechModel");
			
			var oSpeechModel = new sap.ui.model.json.JSONModel();
			oSpeechModel.setData({
				Conversation: []
			});
			sap.ui.getCore().setModel(oSpeechModel, "speechFeed");

			this.getSpeechDialog();
			this.recognition = new webkitSpeechRecognition();
			this.recognition.continuous = false;
			this.recognition.interimResults = true;
			this.recognition.lang = "de-DE";

			this.recognition.onresult = function(event) {
				var interimTranscript = "";
				var finalTranscript = "";
				// this.addConsoleLog("Ergebnis: " + event);
				for (var i = event.resultIndex; i < event.results.length; ++i) {
					if (event.results[i].isFinal) {
						finalTranscript += event.results[i][0].transcript;
					} else {
						interimTranscript += event.results[i][0].transcript;
					}
				}
				//this.addConsoleLog("interim: " + interimTranscript);
				if (finalTranscript) {
					this.addConsoleLog("Request: " + finalTranscript);
					this.transformInput(finalTranscript);
				}

			}.bind(this);

			this.recognition.onstart = function() {
				this.addConsoleLog("Start recording.");
			}.bind(this);

			this.recognition.onend = function(e) {
				// this.transformInput("Wie ist das Wetter in Reutlingen?");
				this.addConsoleLog("End recording.");
			}.bind(this);

			this.recognition.onerror = function(event) {
				this.addConsoleLog(event.error);
			}.bind(this);

		},

		getSpeechDialog: function() {
			if (!this.oDialog) {
				this.oDialog = sap.ui.xmlfragment("com.quanto.solutions.qnnect.fragment.SpeechDialog", this);
			}
			this.oDialog.open();
		},

		startRecognition: function() {
			this.recognition.start();
		},

		stopRecognition: function() {
			this.recognition.stop();
		},

		addConsoleLog: function(sNewLogEntry) {
			/*var oModel = sap.ui.getCore().getModel("speechModel");
			var console = oModel.getProperty("/console");
			oModel.setProperty("/console", console);
			sap.ui.getCore().setModel(oModel, "speechModel");*/
			this.addFeedItem(sNewLogEntry);
		},
		
		addFeedItem: function(sNewFeedItem) {
			var oModel = sap.ui.getCore().getModel("speechFeed");

			var oEntry = {
				Author: "Author",
				AuthorPicUrl: "",
				Type: "Type",
				Date: "",
				Text: sNewFeedItem
			};
			
			var aEntries = oModel.getData().Conversation;
			aEntries.unshift(oEntry);
			oModel.setData({ Conversation: aEntries });
		},

		onPressClose: function() {
			this.recognition.stop();
			this.oDialog.close();
		},

		transformInput: function(text) {

			$.ajax({
				type: "POST",
				url: "https://api.api.ai/v1/query?v=20150910",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				headers: {
					"Authorization": "Bearer " + "15d742a6812b430e98a042b920192e01"
				},
				data: JSON.stringify({
					query: text,
					lang: "en",
					sessionId: "somerandomthing"
				}),

				success: function(data) {
					this.addConsoleLog("Response: " + data.result.fulfillment.speech);
					//this.speak(data.result.fulfillment.speech);
				}.bind(this),
				error: function() {
					this.addConsoleLog("Internal Server Error");
				}.bind(this)
			});
		},

		speak: function(textToSpeach) {
			var oSpeechSynthesisUtterance = new SpeechSynthesisUtterance(textToSpeach);
			oSpeechSynthesisUtterance.lang = "de-DE";
			window.speechSynthesis.speak(oSpeechSynthesisUtterance);
		}
	});
});