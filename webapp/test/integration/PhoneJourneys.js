jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/quanto/solutions/qnnect/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/quanto/solutions/qnnect/test/integration/pages/App",
	"com/quanto/solutions/qnnect/test/integration/pages/Browser",
	"com/quanto/solutions/qnnect/test/integration/pages/Master",
	"com/quanto/solutions/qnnect/test/integration/pages/Detail",
	"com/quanto/solutions/qnnect/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.quanto.solutions.qnnect.view."
	});

	sap.ui.require([
		"com/quanto/solutions/qnnect/test/integration/NavigationJourneyPhone",
		"com/quanto/solutions/qnnect/test/integration/NotFoundJourneyPhone",
		"com/quanto/solutions/qnnect/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});