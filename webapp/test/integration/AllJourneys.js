jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 Devices in the list
// * All 3 Devices have at least one Params

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/quanto/solutions/qnnect/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/quanto/solutions/qnnect/test/integration/pages/App",
	"com/quanto/solutions/qnnect/test/integration/pages/Browser",
	"com/quanto/solutions/qnnect/test/integration/pages/Master",
	"com/quanto/solutions/qnnect/test/integration/pages/Detail",
	"com/quanto/solutions/qnnect/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.quanto.solutions.qnnect.view."
	});

	sap.ui.require([
		"com/quanto/solutions/qnnect/test/integration/MasterJourney",
		"com/quanto/solutions/qnnect/test/integration/NavigationJourney",
		"com/quanto/solutions/qnnect/test/integration/NotFoundJourney",
		"com/quanto/solutions/qnnect/test/integration/BusyJourney",
		"com/quanto/solutions/qnnect/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});